#!/usr/bin/env python3
# ttestcompany@mail.ru
import sys
import numpy as np
import urllib
from urllib.request import urlopen, Request
from bs4 import BeautifulSoup
import requests
from requests.exceptions import HTTPError
from requests.exceptions import ConnectionError
from fake_useragent import UserAgent
from pprint import pprint
import time
import random

import json
import simplejson

from TorCrawler import TorCrawler


# proxiesURL = "https://hidemyna.me/ru/proxy-list/?country=RU&type=hs&anon=34#list"
# proxiesURL = "http://spys.one/proxies/"
# proxiesURL = "http://free-proxy.cz/ru/proxylist/country/RU/http/ping/level2"
# proxiesURL = "http://proxies.su/?proxy_type=http&proxy_type=https&country=RU"

proxiesURL = "https://www.us-proxy.org/"
userAgentURL = "http://www.useragentstring.com/pages/useragentstring.php?name=Chrome"
userAgentURL = "https://developers.whatismybrowser.com/useragents/explore/software_name/chrome/"

hhUrl = "https://hh.ru"
baseUrl = "https://hh.ru/search/resume?"

urlRegionsNames = "https://api.hh.ru/areas"
urlLanguages = "https://api.hh.ru/languages"

urlSpec = "https://api.hh.ru/specializations"
filterData = "https://api.hh.ru/dictionaries"

ageArray = [[18,30],[14,18],[30,40],[40,50],[50,60],[60]]
employmentArray = ["full", "part", "project", "probation", "volunteer"]
experienceArray =["moreThan6", "between3And6", "between1And3","noExperience"] 
genderArray = ["male", "female"] 
scheduleArray = ["fullDay", "shift", "flexible", "remote", "flyInFlyOut"] 
driverLicenseArray=["A","B","C","D","E","CE","BE","DE","TM","TB"] 

educationLevelArray = ["higher","bachelor","master","candidate","doctor","unfinished_higher","secondary","special_secondary"]
relocationTypeArray = ["no_relocation","relocation_possible","relocation_desirable"]
preferred_ContactTypeArray = ["home","work","cell","email"]
negotiations_order = ["updated_at","created_at"]


delay = random.uniform(1,4)

uAgent = UserAgent()

def parser(url, params='', headers='', proxy=None):
	global proxiesList
	proxies = None
	if proxy is not None:
		proxies = {
			"http": "http://"+proxy,
			"https":"http://"+proxy
		}
	try:
		dataFromUrl=requests.get(url=url, headers=headers, params=params, proxies=proxies)
		#pprint.pprint(dataFromUrl.status_code)
		if(dataFromUrl.status_code ==200):
			data = json.loads(dataFromUrl.text)
			return data
		else:
			print(dataFromUrl.status_code)
			proxy = random.choice(proxiesList)
			return parser(url, params, headers, proxy)
			# return None
	except ConnectionError:
		print("ConnectionError_1")
		# Remove wrong proxy
		if proxy in proxiesList:
			proxiesList.remove(proxy)
		if len(proxiesList) ==0:			
			proxiesList = getproxiesList(proxiesURL, userAgent)
			proxy = random.choice(proxiesList)
		# Get new proxy
		proxy = random.choice(proxiesList)
		# Start functiont again
		return parser(url,params,header,proxy)


def get_html(url, userAgent=None):
	print("I'm in <<get_html>> function")
	url = url
	userAgent = userAgent or None
	try:
		response = urlopen(Request(url, headers=userAgent))
		# response = requests.get(url=url, headers=userAgent)
		status = response.getcode()
		return response.read(), int(status)
	except urllib.error.HTTPError:
		status = 404
		pprint("IT IS HTTP ERROR!!!")
		return [], 404
	except (RuntimeError, TypeError):
		newProxyConnection()
		userAgent = {'User-Agent': uAgent.random}
		# time.sleep(120)
		return get_html(url, userAgent)
		
def get_html_v2(url, proxy, userAgent=''):
	global proxiesList
	print("proxiesList length:", len(proxiesList))
	time.sleep(delay)
	print(proxy)
	# print(userAgent)
	data = None
	status = None
	url = url
	userAgent = userAgent or ''
	proxies = {
		"http": "http://"+proxy,
		"https":"http://"+proxy
	}
	try:
		print('try')
		# Get a request
		response = requests.get(url=url, headers=userAgent, proxies=proxies)
		print(type(response.text))
		# Get a status of the request
		status = response.status_code
		print(status)
		# Return request values and status
		return response.text, int(status), proxy
	# If proxy doesn't work
	except ConnectionError:
		print("ConnectionError_2")
		# Remove wrong proxy
		proxiesList.remove(proxy)
		if len(proxiesList) ==0:
			proxiesList = getproxiesList(proxiesURL, userAgent)
			proxy = random.choice(proxiesList)
		# Get new proxy
		proxy = random.choice(proxiesList)
		# Start function again
		return get_html_v2(url, userAgent=userAgent, proxy=proxy)
		# get_html_v2(url, userAgent=userAgent, proxy='45.33.91.82:8080')
	# If proxy is banned
	except (RuntimeError, TypeError):
		print('except_2')
		# Get new proxy
		proxy = random.choice(proxiesList)
		# Get new User Agent
		userAgent = {'User-Agent': uAgent.random}
		# Sleep some times
		# time.sleep(30)
		# Start function again
		return get_html_v2(url, userAgent=userAgent, proxy=proxy)
	except Exception as e:
		print('except_3')
		print(e)
		return [], 404
		# # print("Unexpected error:", sys.exc_info()[0])
		# proxy = random.choice(proxiesList)
		# status = 404
		# pprint("IT IS HTTP ERROR!!!")
		# get_html_v2(url, userAgent, proxy)
		# # return [], 404


def getproxiesList(url, userAgent):
	print("Proxies-List START")
	proxiesList = []
	http = "http"
	response = urlopen(Request(url, headers=userAgent)).read()
	soup = BeautifulSoup(response, 'lxml')
	for data in soup.find("tbody").find_all("tr"):
		ip = data.find_all("td")[0].text
		port = data.find_all("td")[1].text
		httpCheck = data.find_all("td")[6].text
		if httpCheck.lower() == "no":
			http = "http"
		elif httpCheck.lower() == "yes":
			http = "https"
		# prx = {http : http+"://"+str(ip)+":"+str(port)}
		prx = str(ip)+":"+str(port)
		proxiesList.append(prx)	
	pprint(len(proxiesList))
	print("Proxies-List END")
	print("--------------------")
	return proxiesList

# https://developers.whatismybrowser.com
def getUserAgentList(url):
	print("User-Agent START")
	userAgentList = []
	response = urlopen(Request(url)).read()
	soup = BeautifulSoup(response, 'lxml')
	for data in soup.find("table", class_="table-useragents").find("tbody").find_all("tr"):
		ua = data.find("a").text
		userAgentList.append(ua)
	pprint(len(userAgentList))
	print("User-Agent FINISH")
	print("--------------------")
	return userAgentList

# # http://www.useragentstring.com
# def getUserAgentList(url):
# 	print("User-Agent START")
# 	userAgentList = []
# 	response = urlopen(Request(url)).read()
# 	soup = BeautifulSoup(response, 'lxml')
# 	for data in soup.find("div", id="liste").find_all("ul"):
# 		ua = data.find("a").text		
# 		userAgentList.append(ua)
# 	# pprint(userAgentList)
# 	print("User-Agent FINISH")
# 	print("--------------------")
# 	return userAgentList

def getRegionsNames(userAgent=None, proxy=None):
	userAgent = userAgent or ''
	proxy = proxy or None
	print("Regions-Names START")
	try:
		allRegions=[]
		regionsNames = parser(urlRegionsNames, headers=userAgent, proxy=proxy)
		for regionName in regionsNames:
			if regionName["name"] == "Россия":
				for areas in regionName["areas"]:
					allRegions.append(areas["id"])
					for area in areas["areas"]:
						allRegions.append(area["id"])
		if allRegions[0] is None:
			proxy = random.choice(proxiesList)
			userAgent = {'User-Agent': uAgent.random}
			return getRegionsNames(userAgent, proxy)
		else:
			print(len(allRegions))
			print("Regions-Names END")
			print("--------------------")
			return allRegions
	except ConnectionError:
		print("Point 1")
		proxy = random.choice(proxiesList)
		userAgent = {'User-Agent': uAgent.random}
		return getRegionsNames(userAgent, proxy)

def getNameOfCategories(userAgent=None, proxy=None):
	userAgent = userAgent or ''
	proxy = proxy or None
	profAreaKeysList = []
	specKeysDict = {}
	print("Name-Of-Categories START")
	try:	
		dataCategories = parser(urlSpec, headers=userAgent, proxy=proxy)
		for mainTitle in dataCategories:
			profAreaKeysList.append(mainTitle["id"])
			# specKeysList[mainTitle["id"]] = list()
			specKeysDict[mainTitle["id"]] = list()
			for title in mainTitle["specializations"]:
				# specKeysList[mainTitle["id"]].append(title["id"])
				specKeysDict[mainTitle["id"]].append(title["id"])
		if specKeysDict is None:
			proxy = random.choice(proxiesList)
			userAgent = {'User-Agent': uAgent.random}
			return getNameOfCategories(userAgent, proxy)
		else: 
			# print(len(specKeysList))
			print("Name-Of-Categories END")
			print("--------------------")
			return profAreaKeysList, specKeysDict
	# except (RuntimeError, TypeError):
	except ConnectionError:
		print("Point 2")
		proxy = random.choice(proxiesList)
		userAgent = {'User-Agent': uAgent.random}
		return getNameOfCategories(userAgent, proxy)

def getHref(html):
	# Open a file where we are going to write all links
	fileCV = open('CVlinks.txt','a')
	urlOnePage = np.array([])
	soup = BeautifulSoup(html, "lxml")
	dataFromBlock = soup.find("div", class_="bloko-column bloko-column_l-13 bloko-column_m-9")
	
	# find all tags with CV links
	for link in soup.find_all("div", class_="resume-search-item__header"):
		href = link.find("a")
		urlOnePage = np.append(urlOnePage, href["href"])
		fileCV.write(href["href"] + '\n')
	return urlOnePage

def getPagesNumber(html):
	print('Getting number of pages')
	soup = BeautifulSoup(html, "lxml")
	pagesNumberTag = soup.find_all("a", {"data-qa" : "pager-page"})
	if len(pagesNumberTag) >0:
		pagesNumber = pagesNumberTag[len(pagesNumberTag)-1].text
		print("Page Number:", pagesNumber)
		return int(pagesNumber)
	else:
		print("Not pagesNumber")
		return 0

def newProxyConnection():
	proxy = {'http': random.choice(proxies)}
	proxy_support = urllib.request.ProxyHandler(proxy)
	opener = urllib.request.build_opener(proxy_support, urllib.request.HTTPHandler(debuglevel=1))
	urllib.request.install_opener(opener)
	print("-------------------------------------------")
	print("___New Proxy__ __Connection was changed!___")
	print("-------------------------------------------")
	return proxy

def getDataCV(html, urlLink):
	dataCV = {}
	positionSpec = list()
	EmplyAndAhedule = list()
	workExp = list()
	keySkills = list()
	education = list()
	lang = list()

	soup = BeautifulSoup(html, "lxml")
	
	genderTag = soup.find("span", {"data-qa" : "resume-personal-gender"})
	gender = None if genderTag is None or len(str(genderTag))==0 else genderTag.text
	
	ageTag = soup.find("span", {"data-qa" : "resume-personal-age"})
	age = None if ageTag is None or len(str(ageTag))==0 else ageTag.text

	birthdayTag = soup.find("meta", {"data-qa" : "resume-personal-birthday"})
	birthday = None if birthdayTag is None or len(str(birthdayTag))==0 else birthdayTag.get("content", None)

	cityTag = soup.find("span", {"data-qa" : "resume-personal-address"})
	city = None if cityTag is None or len(cityTag)==0 else cityTag.text

	metroTag = soup.find("span", {"data-qa" : "resume-personal-metro"})
	metro = None if metroTag is None or len(metroTag)==0 else metroTag.text

	positionTag = soup.find("span", {"data-qa" : "resume-block-title-position"})
	position = None if positionTag is None or len(positionTag)==0 else positionTag.text

	salaryTag = soup.find("span", {"data-qa" : "resume-block-salary"})
	salary = None if salaryTag is None or len(salaryTag)==0 else salaryTag.text

	if soup.find("div", class_="resume-block") is not None and \
		soup.find("div", class_="resume-block").find("div", class_="bloko-column bloko-column_xs-4 bloko-column_s-8 bloko-column_m-9 bloko-column_l-12") is not None and \
		soup.find("div", class_="resume-block").find("div", class_="bloko-column bloko-column_xs-4 bloko-column_s-8 bloko-column_m-9 bloko-column_l-12").find_all("p") is not None:
		for item in soup.find("div", class_="resume-block").find("div", class_="bloko-column bloko-column_xs-4 bloko-column_s-8 bloko-column_m-9 bloko-column_l-12").find_all("p"):
			if item is not None:
				EmplyAndAhedule.append(item.text)

	if soup.find_all("li", {"data-qa" : "resume-block-position-specialization"}) is not None:
		for item in soup.find_all("li", {"data-qa" : "resume-block-position-specialization"}):
			if item is not None:
				positionSpec.append(item.text)

	if soup.find("div", {"data-qa":"resume-block-experience"}) is not None and \
		soup.find("div", {"data-qa":"resume-block-experience"}).find_all("div", class_="resume-block-item-gap") is not None:
		for item in soup.find("div", {"data-qa":"resume-block-experience"}).find_all("div", class_="resume-block-item-gap"):
			work = {}
			if item is not None:
				data = item.find_all("div", class_="bloko-column")
				work["date"] = None if data[0] is None or len(data[0])==0 else data[0].text
				work["jobDescr"] = None if data[1] is None or len(data[1])==0 else data[1].text
				workExp.append(work)

	if soup.find("div", {"data-qa":"skills-table"}) is not None and \
		soup.find("div", {"data-qa":"skills-table"}).find_all("div", class_="resume-block-container") is not None:
		for item in soup.find("div", {"data-qa":"skills-table"}).find_all("div", class_="resume-block-container"):
			if item.find("div") is not None:
				for value in item.find("div").find_all("span", {"data-qa":"bloko-tag__text"}):
					keySkills.append(value.text)

	aboutMeTag = soup.find("div", {"data-qa":"resume-block-skills"})
	aboutMe = None if aboutMeTag is None or len(aboutMeTag)==0 else aboutMeTag.text

	if soup.find("div", {"data-qa":"resume-block-education"}) is not None and \
		soup.find("div", {"data-qa":"resume-block-education"}).find_all("div", {"data-qa":"resume-block-education-item"}) is not None:
		for item in soup.find("div", {"data-qa":"resume-block-education"}).find_all("div", {"data-qa":"resume-block-education-item"}):
			educItem = {}
			if item is not None:
				educNameTag = item.find("div", {"data-qa": "resume-block-education-name"})
				educOrgTag = item.find("div", {"data-qa": "resume-block-education-organization"})
				educItem["name"] = None if educNameTag is None or len(educNameTag)==0 else educNameTag.text
				educItem["organization"] = None if educOrgTag is None or len(educOrgTag)==0 else educOrgTag.text
				education.append(educItem)

	if soup.find("div", {"data-qa": "resume-block-languages"}) is not None and \
		soup.find("div", {"data-qa": "resume-block-languages"}).find_all("p", {"data-qa":"resume-block-language-item"}) is not None:
		for item in soup.find("div", {"data-qa": "resume-block-languages"}).find_all("p", {"data-qa":"resume-block-language-item"}):
			if item is not None:
				lang.append(item.text)


	# print("gender: ", gender)
	# print("age: ", age)
	# print("birthday: ", birthday)
	# print("city: ", city)
	# print("metro: ", metro)

	# print("position: ", position)
	# print("positionSpec: ", positionSpec)
	# print("salary: ", salary)

	# print("\n EmplyAndAhedule")
	# pprint(EmplyAndAhedule)
	# print("\n workExp")
	# pprint(workExp)
	# print("\n keySkills")
	# pprint(keySkills)
	# print("\n aboutMe")
	# pprint(aboutMe)
	# print("\n education")
	# pprint(education)
	# print("\n lang")
	# pprint(lang)

	dataCV["url"] = urlLink
	dataCV["gender"] = gender
	dataCV["age"] = age
	dataCV["birthday"] = birthday
	dataCV["city"] = city
	dataCV["metro"] = metro
	dataCV["position"] = position
	dataCV["positionSpec"] = positionSpec
	dataCV["salary"] = salary
	dataCV["EmplyAndAhedule"] = EmplyAndAhedule
	dataCV["workExp"] = workExp
	dataCV["keySkills"] = keySkills
	dataCV["aboutMe"] = aboutMe
	dataCV["education"] = education
	dataCV["lang"] = lang

	return dataCV

def getUrlCV(urlCVArray, page):
	cityParam = ""
	specializationParam = ""
	ageFromParam = ""
	ageToParam = ""
	genderParam = ""
	educationLevelParam = ""
	employmentParam = ""
	experienceParam = ""
	scheduleParam = ""

	# Delay for the request
	delay = random.uniform(1,3)

	print("Point 3")
	# userAgent = {'User-Agent': random.choice(userAgents)}

	# Define User Agent
	userAgent = {'User-Agent': uAgent.random}
	# Get proxy
	proxy = random.choice(proxiesList)

	# fileCV = open('CVlinks.txt','a')
	# Shuffle arrays
	random.shuffle(listOfCity)
	random.shuffle(listOfProfArea)
	random.shuffle(genderArray)
	for city in listOfCity:
		for profArea in listOfProfArea:
		# for spec in listOfSpec[profArea]:
			for gender in genderArray:
				try:
					page = 0
					# Print number of the page
					print("Page: ", page)
					
					cityParam = "area="+city
					specializationParam = "specialization="+str(profArea)
					genderParam = "gender="+gender

					# Print full link
					print(baseUrl+cityParam+"&"+specializationParam+"&"+genderParam+"&"+ "page="+str(page))

					data, status, proxy = get_html_v2(baseUrl+
									cityParam+
									"&"+specializationParam+
									"&"+genderParam+
									"&"+ "page="+str(page),
									userAgent=userAgent,
									proxy=proxy
					)

					# Вариант когда data = None и функция getPagesNumber() - не сработает
					pageNumbers = getPagesNumber(data)
					print(pageNumbers)
					# print("proxiesList length:", len(proxiesList))
					if pageNumbers >= 250:
						for spec in listOfSpec[profArea]:
							specializationParam = "specialization="+str(spec)
							
							data, status, proxy = get_html_v2(baseUrl+
									cityParam+
									"&"+specializationParam+
									"&"+genderParam+
									"&"+ "page="+str(page),
									userAgent=userAgent,
									proxy=proxy
							)

							pageNumbers = getPagesNumber(data)
							print(pageNumbers)
							if pageNumbers >= 250:
								for education_level in educationLevelArray:
									educationLevelParam = "education_level="+education_level

									data, status, proxy = get_html_v2(baseUrl+
													cityParam+
													"&"+specializationParam+
													"&"+genderParam+
													"&"+educationLevelParam+
													"&"+ "page="+str(page),
													userAgent=userAgent,
													proxy=proxy
									)

									pageNumbers = getPagesNumber(data)
									print(pageNumbers)
									if pageNumbers >= 250:
										for employment in employmentArray:
											employmentParam = "employment="+employment
																					
											data, status, proxy = get_html_v2(baseUrl+
															cityParam+
															"&"+specializationParam+
															"&"+genderParam+
															"&"+educationLevelParam+
															"&"+employmentParam+
															"&"+ "page="+str(page),
															userAgent=userAgent,
															proxy=proxy
											)

											pageNumbers = getPagesNumber(data)
											print(pageNumbers)
											if pageNumbers >= 250:
												for experience in experienceArray:
													experienceParam = "experience="+experience

													data, status, proxy = get_html_v2(baseUrl+
																	cityParam+
																	"&"+specializationParam+
																	"&"+genderParam+
																	"&"+educationLevelParam+
																	"&"+employmentParam+
																	"&"+experienceParam+
																	"&"+ "page="+str(page),
																	userAgent=userAgent,
																	proxy=proxy
													)

													pageNumbers = getPagesNumber(data)
													print(pageNumbers)
													if pageNumbers >= 250:
														for schedule in scheduleArray:
															scheduleParam = "schedule="+schedule

															data, status, proxy = get_html_v2(baseUrl+
																			cityParam+
																			"&"+specializationParam+
																			"&"+genderParam+
																			"&"+educationLevelParam+
																			"&"+employmentParam+
																			"&"+experienceParam+
																			"&"+scheduleParam+
																			"&"+ "page="+str(page),
																			userAgent=userAgent,
																			proxy=proxy
															)

															pageNumbers = getPagesNumber(data)
															print(pageNumbers)
															if pageNumbers >= 250:
																for age in ageArray:

																	if len(age)>1:
																		ageFromParam = "age_from="+str(age[0])
																		ageToParam = "age_to="+str(age[1])
																	else:
																		ageFromParam = "age_from="+str(age[0])
																		ageToParam  = ""

																	data, status, proxy = get_html_v2(baseUrl+
																					cityParam+
																					"&"+specializationParam+
																					"&"+genderParam+
																					"&"+educationLevelParam+
																					"&"+employmentParam+
																					"&"+experienceParam+
																					"&"+scheduleParam+
																					"&"+ageFromParam+
																					"&"+ageToParam+
																					"&"+ "page="+str(page),
																					userAgent=userAgent,
																					proxy=proxy
																	)

																	pageNumbers = getPagesNumber(data)
																	print(pageNumbers)
																	if pageNumbers >= 250:
																		salaryFrom = 0
																		salaryTo = 50000
																		salaryFromParam = "salary_from="+salaryFrom
																		salaryToParam = "salary_to="+salaryTo

																		while salaryTo < 5000000:
																			page = 0
																			data, status, proxy = get_html_v2(baseUrl+
																						cityParam+
																						"&"+specializationParam+
																						"&"+genderParam+
																						"&"+educationLevelParam+
																						"&"+employmentParam+
																						"&"+experienceParam+
																						"&"+scheduleParam+
																						"&"+ageFromParam+
																						"&"+ageToParam+
																						"&"+salaryFromParam+
																						"&"+salaryToParam+
																						"&"+ "page="+str(page),
																						userAgent=userAgent,
																						proxy=proxy
																			)
																			if status == 200:
																				urls = getHref(data)
																				urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
																			while(status == 200 and len(urls)>0):
																				print(baseUrl+
																					cityParam+
																					"&"+specializationParam+
																					"&"+genderParam+
																					"&"+educationLevelParam+
																					"&"+employmentParam+
																					"&"+experienceParam+
																					"&"+scheduleParam+
																					"&"+ageFromParam+
																					"&"+ageToParam+
																					"&"+salaryFromParam+
																					"&"+salaryToParam+
																					"&"+ "page="+str(page)
																				)	
																				print("Page: ", page)
																				print("Length: ", len(urlCVArray))
																				print("--------------------")
																				page += 1
																				# time.sleep(delay)
																				data, status, proxy = get_html_v2(baseUrl+
																								cityParam+
																								"&"+specializationParam+
																								"&"+genderParam+
																								"&"+educationLevelParam+
																								"&"+employmentParam+
																								"&"+experienceParam+
																								"&"+scheduleParam+
																								"&"+ageFromParam+
																								"&"+ageToParam+
																								"&"+salaryFromParam+
																								"&"+salaryToParam+
																								"&"+ "page="+str(page),
																								userAgent=userAgent,
																								proxy=proxy
																				)
																				if status == 200:
																					urls = getHref(data)
																					urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
																					# fileCV.write(urls + '\n')
																			salaryFrom += 50000
																			salaryTo += 50000
																	else:
																		print("Status: ", status)
																		if status == 200:
																			urls = getHref(data)
																			urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
																			while(status == 200 and len(urls)>0):
																				page += 1
																				print("Count iteraciy: ", page)
																				print("---------")
																				print(baseUrl+
																					cityParam+
																					"&"+specializationParam+
																					"&"+genderParam+
																					"&"+educationLevelParam+
																					"&"+employmentParam+
																					"&"+experienceParam+
																					"&"+scheduleParam+
																					"&"+ageFromParam+
																					"&"+ageToParam+
																					"&"+ "page="+str(page)
																				)
																				print("Page: ", page)
																				print("Length: ", len(urlCVArray))
																
																				# time.sleep(delay)
																				data, status, proxy = get_html_v2(baseUrl+
																								cityParam+
																								"&"+specializationParam+
																								"&"+genderParam+
																								"&"+educationLevelParam+
																								"&"+employmentParam+
																								"&"+experienceParam+
																								"&"+scheduleParam+
																								"&"+ageFromParam+
																								"&"+ageToParam+
																								"&"+ "page="+str(page),
																								userAgent=userAgent,
																								proxy=proxy
																				)
																				if status == 200:
																					urls = getHref(data)
																					urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
																					# fileCV.write(urls + '\n')
															else:
																print("Status: ", status)
																if status == 200:
																	urls = getHref(data)
																	urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
																	while(status == 200 and len(urls)>0):
																		page += 1
																		print("Count iteraciy: ", page)
																		print("---------")
																		print(baseUrl+
																			cityParam+
																			"&"+specializationParam+
																			"&"+genderParam+
																			"&"+educationLevelParam+
																			"&"+employmentParam+
																			"&"+experienceParam+
																			"&"+scheduleParam+
																			"&"+ "page="+str(page)
																		)
																		print("Page: ", page)
																		print("Length: ", len(urlCVArray))
														
																		# time.sleep(delay)
																		data, status, proxy = get_html_v2(baseUrl+
																						cityParam+
																						"&"+specializationParam+
																						"&"+genderParam+
																						"&"+educationLevelParam+
																						"&"+employmentParam+
																						"&"+experienceParam+
																						"&"+scheduleParam+
																						"&"+ "page="+str(page),
																						userAgent=userAgent,
																						proxy=proxy
																		)
																		if status == 200:
																			urls = getHref(data)
																			urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
																			# fileCV.write(urls + '\n')
													else:
														print("Status: ", status)
														if status == 200:
															urls = getHref(data)
															urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
															while(status == 200 and len(urls)>0):
																page += 1
																print("Count iteraciy: ", page)
																print("---------")
																print(baseUrl+
																	cityParam+
																	"&"+specializationParam+
																	"&"+genderParam+
																	"&"+educationLevelParam+
																	"&"+employmentParam+
																	"&"+experienceParam+
																	"&"+ "page="+str(page)
																)
																print("Page: ", page)
																print("Length: ", len(urlCVArray))
												
																# time.sleep(delay)
																data, status, proxy = get_html_v2(baseUrl+
																				cityParam+
																				"&"+specializationParam+
																				"&"+genderParam+
																				"&"+educationLevelParam+
																				"&"+employmentParam+
																				"&"+experienceParam+
																				"&"+ "page="+str(page),
																				userAgent=userAgent,
																				proxy=proxy
																)
																if status == 200:
																	urls = getHref(data)
																	urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
																	# fileCV.write(urls + '\n')	
											else:
												print("Status: ", status)
												# raise(RuntimeError)
												if status == 200:
													urls = getHref(data)
													urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
													while(status == 200 and len(urls)>0):
														page += 1
														print("Count iteraciy: ", page)
														print("---------")
														print(baseUrl+cityParam+
															"&"+specializationParam+
															"&"+genderParam+
															"&"+educationLevelParam+
															"&"+employmentParam+
															"&"+ "page="+str(page)
														)
														print("Page: ", page)
														print("Length: ", len(urlCVArray))
										
														# time.sleep(delay)
														data, status, proxy = get_html_v2(baseUrl+
																		cityParam+
																		"&"+specializationParam+
																		"&"+genderParam+
																		"&"+educationLevelParam+
																		"&"+employmentParam+
																		"&"+ "page="+str(page),
																		userAgent=userAgent,
																		proxy=proxy
														)
														if status == 200:
															urls = getHref(data)
															urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
															# fileCV.write(urls + '\n')					
									else:
										print("Less then 250")
										print("Status: ", status)
										# raise(RuntimeError)
										if status == 200:
											urls = getHref(data)
											urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
											while(status == 200 and len(urls)>0):
												page += 1
												print("---------")
												print("Count iteraciy: ", page)
												print(baseUrl+cityParam+
														"&"+specializationParam+
														"&"+genderParam+
														"&"+educationLevelParam+
														"&"+ "page="+str(page)
													)
												print("Page: ", page)
												print("Length: ", len(urlCVArray))
										
												# time.sleep(delay)
												data, status, proxy = get_html_v2(baseUrl+
																		cityParam+
																		"&"+specializationParam+
																		"&"+genderParam+
																		"&"+educationLevelParam+
																		"&"+ "page="+str(page),
																		userAgent=userAgent,
																		proxy=proxy
																)
												if status == 200:
													urls = getHref(data)
													urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
													# fileCV.write(urls + '\n')						
							else:
								print("Less then 250")
								print("Status: ", status)
								# raise(RuntimeError)
								if status == 200:
									urls = getHref(data)
									urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
									while(status == 200 and len(urls)>0):
										page += 1
										print("---------")
										print("Count iteraciy: ", page)
										print(baseUrl+cityParam+
												"&"+specializationParam+
												"&"+genderParam+
												"&"+ "page="+str(page)
											)
										print("Page: ", page)
										print("Length: ", len(urlCVArray))

										# time.sleep(delay)
										data, status, proxy = get_html_v2(baseUrl+
																cityParam+
																"&"+specializationParam+
																"&"+genderParam+													
																"&"+ "page="+str(page),
																userAgent=userAgent,
																proxy=proxy
														)
										if status == 200:
											urls = getHref(data)
											urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
											# fileCV.write(urls + '\n')
					else:
						print("Less then 250")
						print("Status: ", status)
						# raise(RuntimeError)
						if status == 200:
							urls = getHref(data)
							urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
							while(status == 200 and len(urls)>0):
								page += 1
								print("---------")
								print("Count iteraciy: ", page)
								print(baseUrl+cityParam+
										"&"+specializationParam+
										"&"+genderParam+
										"&"+ "page="+str(page)
									)
								print("Page: ", page)
								print("Length: ", len(urlCVArray))

								# time.sleep(delay)
								data, status, proxy = get_html_v2(baseUrl+
														cityParam+
														"&"+specializationParam+
														"&"+genderParam+													
														"&"+ "page="+str(page),
														userAgent=userAgent,
														proxy=proxy
												)
								if status == 200:
									urls = getHref(data)
									urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
									# fileCV.write(urls + '\n')
				# except (RuntimeError, TypeError):
				except (RuntimeError):
					print("I'm in the except")
					# newProxyConnection()
					print("Point 4")
					# userAgent = {'User-Agent': random.choice(userAgents)}
					userAgent = {'User-Agent': uAgent.random}
					# Get new proxy
					proxy = random.choice(proxiesList)
					# time.sleep(200)
					print("Page: ", page)
					print("status:", status)
					if status == 200:
						urls = getHref(data)
						urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
						while(status == 200 and len(urls)>0):
							print(baseUrl+
								cityParam+
								"&"+specializationParam+
								"&"+ageFromParam+
								"&"+ageToParam+
								"&"+genderParam+
								"&"+educationLevelParam+
								"&"+employmentParam+
								"&"+experienceParam+
								"&"+scheduleParam+
								"&"+ "page="+str(page)
							)

							data, status, proxy = get_html_v2(baseUrl+
											cityParam+
											"&"+specializationParam+
											"&"+ageFromParam+
											"&"+ageToParam+
											"&"+genderParam+
											"&"+educationLevelParam+
											"&"+employmentParam+
											"&"+experienceParam+
											"&"+scheduleParam+
											"&"+ "page="+str(page),
											userAgent=userAgent,
											proxy=proxy
							)
							page += 1
							time.sleep(delay)
							if status == 200:
								urls = getHref(data)
								urlCVArray = np.concatenate((urlCVArray, urls), axis=None)
					continue
	fileCV.close()
	return urlCVArray


# Функция для проверки на изменение IP и UserAgent
def get_ip(html):
	# Cайт для проверки IP и UserAgent
	# url = "http://sitespy.ru/my-ip"
	soup = BeautifulSoup(html, 'lxml')
	ip = soup.find('span', class_='ip').text.strip()
	ua = soup.find('span', class_='ip').find_next_sibling('span').text.strip()
	print('ip: '+ip)
	print('ua: '+ua)


def getCVLinksTor(response):
	fileCV = open('CVlinks.txt','a')
	urlOnePage = np.array([])
	for link in response.find_all("div", class_="resume-search-item__header"):
		href = link.find("a")
		urlOnePage = np.append(urlOnePage, href["href"])
		fileCV.write(href["href"] + '\n')
	fileCV.close()
	return urlOnePage



def main():
	
	global userAgents
	global proxiesList

	userAgents = getUserAgentList(userAgentURL)
	# userAgent = {'User-Agent': random.choice(userAgents)}
	userAgent = {'User-Agent': uAgent.random}
	proxiesList = getproxiesList(proxiesURL, userAgent)
	proxy = random.choice(proxiesList)
	# print(len(proxiesList))

	global listOfProfArea
	global listOfSpec
	listOfProfArea, listOfSpec = getNameOfCategories()
	# pprint(listOfSpec)

	time.sleep(5)

	global listOfCity 
	listOfCity = getRegionsNames()
	# pprint(listOfCity)

	urlCVArray = np.array([])
	page = 0
	allLinkCV = getUrlCV(urlCVArray, page)
	# try:
	# 	allLinkCV = getUrlCV(urlCVArray, page)
	# except Exception as e:
	# 	print(e)

	allLinkCV = list()
	allLinkCV = list(set(allLinkCV))

	# f = open("CVlinks.txt", "r")
	# urlLinks = f.readlines()
	# # pprint(urlLinks)
	# # print(type(urlLinks))

	proxy = random.choice(proxiesList)

	file = open("cvData.json","a")
	for link in allLinkCV:
	# for link in urlLinks:
		try:
			print("Get Data From link: ", str(link))
			data, status, proxy = get_html_v2(hhUrl+link, userAgent=userAgent, proxy=proxy)
			if status == 200:
				dataCV = getDataCV(data, hhUrl+link)
				file.write(simplejson.dumps(dataCV, ensure_ascii=False))
				file.write('\n')
				time.sleep(delay)
		except:
			print("Point 5")
			proxy = random.choice(proxiesList)
			userAgent = {'User-Agent': uAgent.random}
			time.sleep(30)
			continue
	file.close()

	



if __name__ == '__main__' :
	main()







	# pprint(proxies)

	# data, status, proxy = get_html_v2("https://hh.ru/search/resume?area=1620&specialization=1.395&gender=male&page=0", userAgent, proxy)
	# pprint(status)

	# newproxiesList = get_html3("https://hh.ru/search/resume?area=1620&specialization=1.395&gender=male&page=0", userAgent, proxy)
	# print("newproxiesList:", str(len(newproxiesList)))

	# links = getHref(data)

	# pprint(links)




"""
age_from=14&
age_to=18&
schedule=fullDay&
employment=full&
experience=between1And3&
gender=male&
salary_from=30000&
salary_to=50000& 

area=1&
specialization=17.149&

clusters=true&
label=only_with_salary&
label=only_with_gender&
language=rus.a1&
no_magic=false&

order_by=relevance&
salary_from=30000&
salary_to=50000&

label=only_with_photo&
from=cluster_label

"""

"""
education_level - уровень образования
preferred_contact_type - желаемый способ связи
relocation_type - готовность к переезду

"""
